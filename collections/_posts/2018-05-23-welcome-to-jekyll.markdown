---
layout: post
url: como-jekyll-tornou-se-o-maior-staticgen
title:  Tudo que precisa saber para criar um site incrível
date:   2016-03-24 15:32:14 -0300
call: Veja como duas técnicas de design vão virar a cabeça de seus cliente e deixá-los babando pelo seu produto.
image: /assets/images/sample.png
alt: Sample
categories: Startup Marketing
---

{% include image.html
src="https://www.telegraph.co.uk/content/dam/Travel/2017/February/europe-beaches-Playa%20de%20Muro%20Beach-AP.jpg"
alt="farm ville" %}

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada varius ipsum et ultrices. Donec sit amet purus lacinia, aliquet massa et, feugiat enim. Phasellus lacinia, libero ut auctor volutpat, risus dui posuere tortor, eu cursus mi lectus id est.

## Vivamus fermentum metus vehicula leo ultricies, a viverra sapien lacinia. 

Aliquam eu dapibus ex. Nullam consequat, purus ac hendrerit dictum, mauris leo dictum diam, sed egestas mauris neque vitae neque. Etiam et arcu non sapien volutpat rhoncus vel a eros.

Aliquam euismod ipsum tellus, nec elementum leo imperdiet quis. In laoreet vel lacus quis fringilla. In aliquet et ex at tempor.

Aenean elit nulla, rutrum a urna a, consequat efficitur justo. Nunc dolor quam, tempus quis eleifend eu, molestie fermentum massa. Sed tristique, risus sit amet porta feugiat, arcu velit ultricies nisi, a pharetra sapien enim quis magna. Mauris nec massa dapibus, tempus nisi nec, sollicitudin sem. Curabitur fermentum pretium rutrum.

Praesent viverra dui eget augue malesuada bibendum. Pellentesque congue accumsan facilisis. Nunc ullamcorper efficitur vestibulum. Morbi consequat nisl turpis.

Integer quis erat nec diam luctus interdum in in ex. In dignissim nisi erat, eu tincidunt risus feugiat et. Pellentesque sagittis massa ac libero molestie scelerisque. Donec id est odio.

## Vivamus fermentum metus vehicula leo ultricies, a viverra sapien lacinia. 

Curabitur vel purus ante. Nulla a nisi ut eros posuere eleifend. Nulla mi ante, posuere nec elit ut, sagittis dapibus eros. Ut ipsum nisl, tempor eget mi convallis, fringilla sagittis eros.

Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Nullam vel nunc odio. Integer dignissim blandit justo, sed placerat mi vulputate et. Quisque ipsum nibh, molestie in arcu ut, tincidunt porta erat. Sed non odio ac massa sollicitudin molestie. Nunc et mauris blandit, vehicula ipsum at, dictum odio.

Morbi ac lobortis enim. Integer nisl mauris, faucibus in eros nec, accumsan auctor lorem. Proin sapien risus, vulputate id quam id, vestibulum tincidunt nisi. Proin tempus justo ultrices, laoreet dui sollicitudin, euismod lectus. Nullam faucibus, leo eget sagittis lobortis, turpis sem porta libero, nec bibendum turpis ex eu metus.

## Vivamus fermentum metus vehicula leo ultricies, a viverra sapien lacinia. 

Maecenas non pretium sapien. Suspendisse sagittis auctor interdum. Aliquam sit amet elit finibus, tempus diam vel, pulvinar magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Mauris scelerisque et nunc in convallis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In tempor, est ut ultrices varius, ex felis dictum massa, nec maximus turpis tellus porta lacus.

Proin turpis turpis, laoreet in nibh in, consequat vulputate eros. Nam id dui nisi. Proin orci urna, rutrum id metus sit amet, varius aliquam mi. Sed gravida iaculis dolor id convallis.

Aenean tincidunt, nibh nec volutpat feugiat, nisi sem eleifend ante, id condimentum sem sem vitae tortor. Fusce vel urna felis. Nulla eget nunc vitae nibh tempor auctor. Maecenas sollicitudin enim in sem convallis feugiat.

