---
layout: page
title: Como Criar Seu Próprio Negócio Sozinho, Do Zero e Usando O Poder Da Internet
---

Seja bem-vindo.

Eu me chamo Rodolfo Maciel e criei este site para resolver o problema acima.

Sim porque já faz algum tempo que eu desejo criar meu próprio negócio digital.

E até já fiz alguns cursos muito conhecidos na área de marketing digital. Mas eu não consegui colocar em prática.

A mensagem era que eles queriam ensinar a desenvolver um negócio online mas o foco estava na direção errada.

Eu senti que estava faltando alguma peça nesse quebra-cabeça, que me atrapalhava de alcançar meus objetivos.

E depois de muita pesquisa eu entendi que para eu criar meu próprio negócio digital eu precisava de três elementos que são essenciais.

São eles:
- Clientes
- Produtos
- Ofertas

Se faltar um desses então você não tem nada.

Por isso estou criando esse site com o objetivo de compartilhar as minhas pesquisas sobre como desenvolver esses três áreas do seu negócio.

Meu objetivo aqui é falar sobre cada um desses tópicos de forma simples, objetiva e concisa para facilitar o entendimento e a prática.

Eu acredito que essa é a melhor forma de aprender.

Por isso fique à vontade para assinar meu site e aproveitar o conteúdo que irei disponibilizar. Você pode fazer isso no formulário abaixo.

Tenho certeza que teremos muita oportunidade de crescer juntos.


{% include subscribe.html %}