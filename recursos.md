---
layout: page
permalink: recursos
---

<ul>
{% for item in site.categories.Recursos %}
    <li class="course">
        <div class="course-header">
            <a href="{{ item.url }}" class="course-link">
                <h2 class="course-title">{{ item.title }}</h2>
                <span class="course-description" >{{ item.call }}</span>
            </a>
        </div>
        <div class="course-image">
            <img src="{{ item.image }}" alt="{{ item.alt }}">
        </div>
    </li>
{% endfor %}
</ul>